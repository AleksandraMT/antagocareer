<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, maximum-scale=1">
    <meta http-equiv="content-language" content="ru"/>
    <meta charset="utf-8">
    <title>Antagosoft - создание и поддержка интернет-проектов</title>
    <meta name="description" content="Создание и поддержка интернет-проектов, разработка сайтов, стартапов">
    <meta name="keywords" content="создание сайтов, разработка интернет стартапов, аутсорсинг web-разработчиков">
    <meta property="og:image" content="http://antagosoft.ru/img/antagosoft.png"/>
    <meta property="og:url" content="http://antagosoft.ru"/>
    <link rel="icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/font.css">
    <link rel="stylesheet" href="css/plugins/jquery.arcticmodal-0.3.min.css">
    <link rel="stylesheet" href="css/plugins/jquery.scrollbar.css">
    <link rel="stylesheet" href="css/scrollbarCustom.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div class="b-loader j-loader">
    <div class="b-loader__text j-loader-hide">
        <span class="b-loader__letter j-loader-show">a</span>
        <span class="b-loader__letter j-loader-show">n</span>
        <span class="b-loader__letter j-loader-show">t</span>
        <span class="b-loader__letter j-loader-show">a</span>
        <span class="b-loader__letter j-loader-show">g</span>
        <span class="b-loader__letter j-loader-show">o</span>
        <span class="b-loader__letter j-loader-show">s</span>
        <span class="b-loader__letter j-loader-show">o</span>
        <span class="b-loader__letter j-loader-show">f</span>
        <span class="b-loader__letter j-loader-show">t</span>
    </div>
</div>
<div class="scrollbar-dynamic j-scroll-custom">
    <header class="b-header">
        <a class="b-sq_logo j-nav-btn" href="#about"></a>
        <nav class="b-nav j-nav" data-open="false">
            <button class="b-nav__btn arrow arrow-up" type="button" role="button" aria-label="Toggle Navigation">
                <span class="b-nav_btn-line"></span>
            </button>
            <ul class="b-nav__list">
                <li class="b-nav__item"><a class="j-nav-btn" href="#services">О нас</a></li>
                <li class="b-nav__item"><a class="j-nav-btn" href="#team">Команда</a></li>
                <li class="b-nav__item"><a class="j-nav-btn" href="#faq">Контакты</a></li>
            </ul>
        </nav>
        <div class="b-info">
            <a href="mailto:antago@i.ua" class="g-mail m-mail_big">antago@i.ua</a>
            <a href="mailto:pm@antagososft.com" class="g-mail m-mail_big">pm@antagosoft.com</a>
            <a href="skype:antagosoft?call" class="g-skype m-skype_big">antagosoft</a>
        </div>
    </header>
    <div class="l-sections js-section">
        <section id="about" class="b-about j-zone" data-visible="false">
            <div class="b-about__wrapper">  
                <div class="l-container b-about__container">
                    <h1 class="b-about__title">Карьера в antagosoft</h1>

                    <div class="b-about__subtitle">Мы&nbsp;делаем веб-приложения, которыми пользуются миллионы людей
                        по&nbsp;всему миру. Ты&nbsp;хочешь присоединиться к&nbsp;нам?
                    </div>
                    <a href="#team" class="b-about__btn j-nav-btn"><span>Хочу к вам в команду</span></a>
                </div>
            </div>    
        </section>
        <section class="b-services j-services-wrap" id="services" data-id="services">
            <div class="b-services__title-wrap">
                <div class="l-container">
                    <h2 class="b-services__title">
                        Мы&nbsp;разрабатываем стартапы на&nbsp;PHP с&nbsp;использованием фреймворков.
                        Предпочитаем Yii2 и&nbsp;Angular
                    </h2>
                </div>
            </div>
            <div class="b-services__wrap m-services__wrap_background j-services">
                <div class="l-polygon m-polygon_top">
                    <img src="img/bg/polygon1.svg">
                </div>
                <div class="l-polygon m-polygon_bottom">
                    <img src="img/bg/polygon2.svg">
                </div>
                <div class="l-container">
                    <div class="b-services__list">
                        <div class="b-services__item">
                            <div class="b-services__pic m-services__pic_pic1"></div>
                            <h3 class="b-services__heading">No&nbsp;bullshit</h3>

                            <p class="b-services__desc">
                                Нам важно, какими проектами заниматься. Мы&nbsp;разрабатываем
                                живые стартапы&nbsp;&mdash; проекты, на&nbsp;которых ты&nbsp;не&nbsp;заскучаешь
                            </p>
                        </div>
                        <div class="b-services__item">
                            <div class="b-services__pic m-services__pic_pic2"></div>
                            <h3 class="b-services__heading">Микроклимат</h3>

                            <p class="b-services__desc">
                                Если не&nbsp;хочешь себя чувствовать винтиком в&nbsp;большой организации&nbsp;&mdash;
                                ты&nbsp;попал по&nbsp;адресу. Теплая ламповая атмосфера прилагается
                            </p>
                        </div>
                        <div class="b-services__item">
                            <div class="b-services__pic m-services__pic_pic3"></div>
                            <h3 class="b-services__heading">Неизбежное развитие</h3>

                            <p class="b-services__desc">
                                Мы&nbsp;научим тебя быть продуктивным. Наши сотрудники развиваются не&nbsp;только
                                профессионально, но&nbsp;и&nbsp;личностно. И&nbsp;почти безболезненно
                            </p>
                        </div>
                        <div class="b-services__item">
                            <div class="b-services__pic m-services__pic_pic4"></div>
                            <h3 class="b-services__heading">Гибкий график работы</h3>

                            <p class="b-services__desc">
                                Мы&nbsp;привыкли опираться на&nbsp;результат. Если для того, чтобы быть продуктивным,
                                тебе нужно приходить позже&nbsp;&mdash; пожалуйста
                            </p>
                        </div>
                        <div class="b-services__item">
                            <div class="b-services__pic m-services__pic_pic5"></div>
                            <h3 class="b-services__heading">Ценим время</h3>

                            <p class="b-services__desc">
                                Мы&nbsp;умеем управлять нашим главным ресурсом&nbsp;&mdash; временем. Именно
                                поэтому у&nbsp;нас, например, никогда не&nbsp;бывает задержек в&nbsp;выплатах&nbsp;ЗП
                            </p>
                        </div>
                        <div class="b-services__item">
                            <div class="b-services__pic m-services__pic_pic6"></div>
                            <h3 class="b-services__heading">Ценим пространство</h3>

                            <p class="b-services__desc">
                                Офис&nbsp;&mdash; наш второй дом. Мы&nbsp;заботимся о&nbsp;том, чтобы здесь было
                                приятно проводить большую часть дня.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="b-services__wrap j-services">
                <div class="b-services__additional">
                    <div class="b-services__tr">
                        <h4>И кроме того:</h4>

                        <div class="b-services__td">
                            <ul class="b-services__services-list">
                                <li class="b-services__list-item">- лучшие практики разработки;</li>
                                <li class="b-services__list-item">- курсы английского;</li>
                                <li class="b-services__list-item">- чай/кофе;</li>
                                <li class="b-services__list-item">- хорошие мониторы Dell;</li>
                                <li class="b-services__list-item">- демократичное руководство;</li>
                                <li class="b-services__list-item">- 8 минут от метро "Научная";</li>
                                <li class="b-services__list-item">- еженедельные доклады;</li>
                                <li class="b-services__list-item m-services__item_no-margin">- открытость к новым идеям;</li>
                                <li class="b-services__list-item">...</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="b-services__title-wrap">
                <div class="l-container">
                    <h2 class="b-services__title">
                        Мы&nbsp;разрабатываем стартапы с&nbsp;людьми, <br/>
                        с&nbsp;которыми хочется работать бок о&nbsp;бок каждый день
                    </h2>
                </div>
            </div>
        </section>
        <section class="b-you">
            <div class="l-container">
                <h2 class="b-you__title">Ты:</h2>

                <div class="b-you__block">
                    <ul class="b-you__list">
                        <li class="b-you__item">
                            выбрал профессию веб-разработчика по&nbsp;зову души,
                            а&nbsp;не&nbsp;в&nbsp;погоне за&nbsp;трендами;
                        </li>
                        <li class="b-you__item">
                            понимаешь, что процесс обучения бесконечен и&nbsp;не&nbsp;наступит то&nbsp;время,
                            когда можно будет сказать себе &laquo;я&nbsp;знаю все, что нужно&raquo; и&nbsp;расслабиться;
                        </li>
                        <li class="b-you__item">
                            по&nbsp;своему опыту знаешь, что процесс развития сложен и&nbsp;не&nbsp;всегда приятен;
                        </li>
                        <li class="b-you__item">
                            согласен, что результат&nbsp;&mdash; лучшее мерило любого труда;
                        </li>
                        <li class="b-you__item">
                            стремишься стать лучшим специалистом в&nbsp;своей отрасли;
                        </li>
                        <li class="b-you__item">
                            понимаешь, что в&nbsp;шаблонное резюме никто даже не&nbsp;будет вчитываться;
                        </li>
                        <li class="b-you__item">
                            любишь работать с&nbsp;людьми, даже если они на&nbsp;тебя не&nbsp;похожи =)
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section class="b-team j-zone" data-visible="false" id="team" data-id="team">
            <div class="g-line-3"></div>
            <div class="b-helmet"></div>
            <div>
                <h2 class="b-team__title">Команда</h2>

                <div class="b-team-gallery j-team-gallery">
                    <div class="b-team-gallery__list j-team-list">
                        <div class="b-team-gallery__item">
                            <div class="b-team-gallery__photo">
                                <img src="img/team/team13.jpg">
                            </div>
                            <div class="b-team-gallery__desc-holder">
                                <p class="b-team-gallery__desc">
                                    Александра
                                    <span>Front-end разработчик</span>
                                </p>
                            </div>
                        </div>
                        <div class="b-team-gallery__item">
                            <div class="b-team-gallery__photo">
                                <img src="img/team/team6.jpg">
                            </div>
                            <div class="b-team-gallery__desc-holder">
                                <p class="b-team-gallery__desc">
                                    Сергей
                                    <span>Back-end разработчик</span>
                                </p>
                            </div>
                        </div>
                        <div class="b-team-gallery__item">
                            <div class="b-team-gallery__photo">
                                <img src="img/team/team2.jpg">
                            </div>
                            <div class="b-team-gallery__desc-holder">
                                <p class="b-team-gallery__desc">
                                    Евгения
                                    <span>QA</span>
                                </p>
                            </div>
                        </div>
                        <div class="b-team-gallery__item">
                            <div class="b-team-gallery__photo">
                                <img src="img/team/team5.jpg">
                            </div>
                            <div class="b-team-gallery__desc-holder">
                                <p class="b-team-gallery__desc">
                                    Дмитрий
                                    <span>Front-end разработчик</span>
                                </p>
                            </div>
                        </div>
                        <div class="b-team-gallery__item">
                            <div class="b-team-gallery__photo">
                                <img src="img/team/team3.jpg">
                            </div>
                            <div class="b-team-gallery__desc-holder">
                                <p class="b-team-gallery__desc">
                                    Ирина
                                    <span>Front-end разработчик</span>
                                </p>
                            </div>
                        </div>
                        <div class="b-team-gallery__item">
                            <div class="b-team-gallery__photo">
                                <img src="img/team/team8.jpg">
                            </div>
                            <div class="b-team-gallery__desc-holder">
                                <p class="b-team-gallery__desc">
                                    Денис
                                    <span>Back-end разработчик</span>
                                </p>
                            </div>
                        </div>
                        <div class="b-team-gallery__item">
                            <div class="b-team-gallery__photo">
                                <img src="img/team/team10.jpg">
                            </div>
                            <div class="b-team-gallery__desc-holder">
                                <p class="b-team-gallery__desc">
                                    Марина
                                    <span>Руководитель проектов</span>
                                </p>
                            </div>
                        </div>
                        <div class="b-team-gallery__item">
                            <div class="b-team-gallery__photo">
                                <img src="img/team/team7.jpg">
                            </div>
                            <div class="b-team-gallery__desc-holder">
                                <p class="b-team-gallery__desc">
                                    Сергей
                                    <span>Back-end разработчик</span>
                                </p>
                            </div>
                        </div>
                        <div class="b-team-gallery__item">
                            <div class="b-team-gallery__photo">
                                <img src="img/team/team16.jpg">
                            </div>
                            <div class="b-team-gallery__desc-holder">
                                <p class="b-team-gallery__desc">
                                    Руслан
                                    <span>COO</span>
                                </p>
                            </div>
                        </div>
                        <div class="b-team-gallery__item">
                            <div class="b-team-gallery__photo">
                                <img src="img/team/team12.jpg">
                            </div>
                            <div class="b-team-gallery__desc-holder">
                                <p class="b-team-gallery__desc">
                                    Евгений
                                    <span>СЕО</span>
                                </p>
                            </div>
                        </div>
                        <div class="b-team-gallery__item j-vacancy" data-vacancy_id="1">
                            <div class="b-team-gallery__position m-team-gallery__position_dark">
                                <img src="img/icon/position.svg">
                                <span>front-end разработчик</span>
                            </div>
                        </div>
                        <div class="b-team-gallery__item j-vacancy" data-vacancy_id="2">
                            <div class="b-team-gallery__position">
                                <img src="img/icon/position.svg">
                                <span>php-разработчик</span>
                            </div>
                        </div>
                        <div class="b-team-gallery__item j-vacancy" data-vacancy_id="3">
                            <div class="b-team-gallery__position m-team-gallery__position_dark">
                                <img src="img/icon/position.svg">
                                <span>senior js-разработчик</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="l-container">
                <div class="b-team__list">
                    <div class="b-team__item">
                        <span class="b-team__num">5</span>
                        <span class="b-team__desc">лет на рынке</span>
                    </div>
                    <div class="b-team__item">
                        <span class="b-team__num">300</span>
                        <span class="b-team__desc">проектов</span>
                    </div>
                    <div class="b-team__item">
                        <span class="b-team__num">10</span>
                        <span class="b-team__desc">профессионалов</span>
                    </div>
                </div>
            </div>
        </section>
        <section class="b-faq" id="faq" data-id="faq">
            <div class="l-polygon m-polygon_top">
                <img src="img/bg/polygon1.svg">
            </div>
            <div class="l-polygon m-polygon_bottom">
                <img src="img/bg/polygon2.svg">
            </div>
            <div class="l-container">
                <h2 class="b-faq__title">FAQ</h2>

                <div class="b-faq__accordion">
                    <div class="b-faq__block j-faq-block">
                        <h3 class="b-faq__question">
                            <span class="b-faq__question-text">Хочу с&nbsp;вами работать. Что мне надо сделать?</span>
                        </h3>

                        <div class="b-faq__answer j-faq__answer">
                            Все просто&nbsp;&mdash; актуализируй свое резюме и&nbsp;отправь его нам на&nbsp;почту
                            <a href="mailto:job@antagosoft.com">job@antagosoft.com</a>. Также ты&nbsp;можешь
                            воспользоваться
                            формой выше, нажав на&nbsp;интересующую позицию в&nbsp;блоке вакансий.
                        </div>
                    </div>
                    <div class="b-faq__block j-faq-block">
                        <h3 class="b-faq__question">
                            <span class="b-faq__question-text">Сколько времени вы&nbsp;рассматриваете резюме?</span>
                        </h3>

                        <div class="b-faq__answer j-faq__answer">
                            Если твое резюме нас заинтересовало, мы, вероятнее всего, дадим ответ в&nbsp;течение суток.
                        </div>
                    </div>
                    <div class="b-faq__block j-faq-block">
                        <h3 class="b-faq__question">
                            <span class="b-faq__question-text">К&nbsp;скольким этапам подбора готовиться?</span>
                        </h3>

                        <div class="b-faq__answer j-faq__answer">
                            В&nbsp;обычном порядке подбор соисткателей проходит в&nbsp;3&nbsp;этапа:
                            <ul class="b-faq__list">
                                <li class="b-faq__item">&mdash;&nbsp;тестовое задание;</li>
                                <li class="b-faq__item">&mdash;&nbsp;техническое скайп-собеседование;</li>
                                <li class="b-faq__item">&mdash;&nbsp;общее собеседование в&nbsp;офисе.</li>
                            </ul>
                            Если все проходит гладко и&nbsp;нет задержек со&nbsp;стороны соискателя,
                            то&nbsp;весь процесс занимает до&nbsp;одной недели.
                        </div>
                    </div>
                    <div class="b-faq__block j-faq-block">
                        <h3 class="b-faq__question">
                            <span class="b-faq__question-text">А&nbsp;зачем мне выполнять тестовое задание?
                            Давайте я&nbsp;к&nbsp;вам сразу приду в&nbsp;офис</span>
                        </h3>

                        <div class="b-faq__answer j-faq__answer">
                            Мы&nbsp;ценим подобное рвение. Опыт показывает, что подобный формат проведения собеседований
                            себя не&nbsp;оправдывает. Чтобы уменьшить вероятность ошибки с&nbsp;нашей стороны
                            и&nbsp;набирать людей, которым с&nbsp;нами по&nbsp;пути,
                            все кандидаты проходят одинаковую процедуру найма.
                        </div>
                    </div>
                    <div class="b-faq__block j-faq-block">
                        <h3 class="b-faq__question">
                            <span class="b-faq__question-text">Я&nbsp;отправлял вам резюме,
                                но&nbsp;не&nbsp;получил ответа :/</span>
                        </h3>

                        <div class="b-faq__answer j-faq__answer">
                            Ежедневно мы&nbsp;получаем многие десятки резюме. К&nbsp;сожалению, у&nbsp;нас нет
                            физической возможности дать ответ на&nbsp;каждое из&nbsp;них, поэтому мы&nbsp;связываемся
                            только с&nbsp;заинтересовавшими нас кандидатами. Будьте именно таким кандидатом.
                        </div>
                    </div>
                    <div class="b-faq__block j-faq-block">
                        <h3 class="b-faq__question">
                            <span class="b-faq__question-text">На&nbsp;что сделать акцент в&nbsp;своем резюме?</span>
                        </h3>

                        <div class="b-faq__answer j-faq__answer">
                            Мы&nbsp;не&nbsp;откроем Америку, если посоветуем тебе быть хотя&nbsp;бы
                            немного креативным. Дай лаконичное описание своему опыту и&nbsp;целям, приложи примеры
                            работ,
                            упомяни, чем живешь и&nbsp;увлекаешься. Не&nbsp;пиши о&nbsp;том, что ты&nbsp;целеустремленный
                            и&nbsp;быстрообучаемый. Скажем по&nbsp;секрету, что таковыми является каждый первый,
                            на&nbsp;чьи резюме мы&nbsp;не&nbsp;даем ответы.
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <footer class="b-footer">
        <div class="l-container">
            <div class="b-footer__title">ТЕПЕРЬ ЗАДАВАЙ ВОПРОСЫ ИЛИ ПРИСЫЛАЙ РЕЗЮМЕ</div>
            <a href="mailto:job@antagosoft.com" class="btn btn-default b-footer__corner">
                <span class="line1"></span>
                <span class="line2"></span>
                <span class="line3"></span>
                <span class="line4"></span>
                job@antagosoft.com
            </a>
        </div>
    </footer>

    <div class="j-template_holder m-hidden">
        <div class="j-form_success_message">
            <h4 class="b-message__title">Спасибо! Ваше сообщение отправлено.</h4>

            <p class="b-message__desc">В течение рабочего дня мы свяжемся с вами</p>
        </div>
        <div class="j-form_error_message">
            <h4 class="b-message__title">Ошибка</h4>

            <p class="b-message__desc">При отправке сообщения возникла ошибка</p>
        </div>
        <div class="j-vacancy_template">
            <div class="b-modal j-modal">
                <div class="b-modal__content">
                    <div class="b-modal__close arcticmodal-close"></div>
                    <div class="j-vacancy_content"></div>
                </div>
                <div class="b-modal__form">
                    <form class="g-form j-form" action="sender.php" method="post">
                        <h4 class="b-modal__title">Присоединяйтесь к нам!</h4>

                        <div class="g-form__group j-group">
                            <input type="text" class="g-form__input j-required" placeholder="Ваше имя"
                                   name="Vacancy[name]">
                            <input type="text" class="g-form__vdata" placeholder="не заполняйте" name="Vacancy[vdata]">
                        </div>
                        <div class="g-form__group j-group">
                            <input type="text" class="g-form__input j-required" placeholder="Ваш e-mail"
                                   name="Vacancy[email]">
                        </div>
                        <div class="g-form__group m-form__group_textarea j-group">
                            <input type="text" class="g-form__input j-required" placeholder="Ссылка на резюме"
                                   name="Vacancy[link]">
                        </div>
                        <button class="g-btn m-btn_orange">Отправить заявку</button>
                    </form>
                    <div class="b-message j-message"></div>
                </div>
            </div>
        </div>
        <div class="j-vacancy_error">
            <div class="b-modal j-modal">
                <div class="b-modal__content">
                    <div class="b-modal__close arcticmodal-close"></div>
                    <div class="j-vacancy_content">Извините, указанная вакансия на данный момент не может быть
                        отображена, можете связаться с нами для уточнения
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript" src="js/plugins/jquery.event.drag-2.2.min.js"></script>
<script type="text/javascript" src="js/plugins/freewall.min.js"></script>
<script type="text/javascript" src="js/plugins/jquery.arcticmodal-0.3.min.js"></script>
<script type="text/javascript" src="js/plugins/jquery.scrollupformenu.js"></script>
<script type="text/javascript" src="js/plugins/jquery.scrollbar.min.js"></script>
<script type="text/javascript" src="js/main.js"></script>
</body>
</html>
