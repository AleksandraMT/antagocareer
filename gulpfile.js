'use strict';

const gulp = require('gulp');
const uncss = require('gulp-uncss');

gulp.task('unCss:build', function () {
    gulp.src(['css/style.css'])
        .pipe(uncss({
            html: ['index.php', '*.html']
        }))
        .pipe(gulp.dest('./css'));
});