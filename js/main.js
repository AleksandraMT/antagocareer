var Main;
Main = {
    $scrollWrap: $('.j-scroll-custom, html, body'),

    init: function () {
        var self = this;

        if (/Android|webOS|iPhone|iPod|iPad|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            $('body').addClass('l-mobile');
        }

        $(window).on('load', function () {
            $('.j-loader-show')
                .addClass('m-loader__letter_step-show')
                .last()
                .on('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd', function() {
                    $('.j-loader').remove();
                });
            $('.j-loader-hide').addClass('m-loader__text_step-hide');
            $('.j-loader').addClass('m-loader_hide');
        });

        if (screen.width < 680) {
            $('header').scrollUpMenu({'transitionTime': 150});
        }

        if ($(window).width() > 760) {
            $('.j-scroll-custom').scrollbar({
                duration: 300,
                scrolly: 'advanced',
                ignoreMobile: true,
                ignoreOverlay: true
            });
        }

        Forms.init();
        Vacancy.init();
        self.bindEvents();

    },
    bindEvents: function () {
        var self = this;
        $(document)
            .on('click', '.j-nav-btn', function () {
                self.scrollToSection($(this.hash));
                if(!$('body').hasClass('l-mobile')) {
                    return false;
                }
            })
            .on('click', '.j-nav', function () {
                var $navButton = $(this).children('button');
                if ($navButton.hasClass('close')) {
                    $navButton.removeClass('close');
                }
                else {
                    $navButton.addClass('close');
                }

                $(this).attr('data-open', ($(this).attr('data-open') == 'false' ? 'true' : 'false'));
            })
            .on('click', function (event) {
                if ($(event.target).closest('.close').length) return;
                var $navMenu =  $('.j-nav');
                $navMenu.children('button').removeClass('close');
                $navMenu.attr('data-open', ($(this).attr('data-open') == 'false' ? 'true' : 'false'));
                event.stopPropagation();
            });

        $('.j-faq-block').on('click', function (event) {
            $('.j-faq__answer').stop().slideUp(200);

            $(this).find('.j-faq__answer').stop().slideToggle(200);
        })
    },
    scrollToSection: function (target) {
        target = target.length ? target : $('[name=' + target.selector.slice(1) +']');
        if (target.length) {
            this.$scrollWrap.stop().animate({
                scrollTop: target.position().top - 60
            }, 1000);
            return false;
        }
    },
    openModal: function ($content) {
        $content.arcticmodal({
            overlay: {
                css: {
                    backgroundColor: '#0086c6',
                    opacity: .9
                }
            }
        });
    }
};

var Vacancy = {
    vacancyTemplate: null,
    vacancyError: null,
    vacancyId: null,
    init: function () {
        var self = this;
        self.vacancyTemplate = $('.j-vacancy_template');
        self.vacancyError = $('.j-vacancy_error');
        $(document).on('click', '.j-vacancy', function () {
            self.vacancyId = $(this).data('vacancy_id');
            console.log(self.vacancyId);
            var url = 'vacancy' + self.vacancyId + '.html';
            self.displayVacancy(url);
        });
    },
    displayVacancy: function (url) {
        var self = this;
        $.ajax({
            url: url,
            method: 'get',
            success: function (response) {
                var vacancy = self.vacancyTemplate;
                vacancy.find('.j-vacancy_content').html(response);
                vacancy.find('.j-form').data('vacancy_id', self.vacancyId);
                Main.openModal(vacancy);
            },
            error: function (response) {
                var error = self.vacancyError;
                Main.openModal(error);
            }
        });
    }
}

var Forms = {
    successMessage: 'message sent successfully',
    errorMessage: 'error, message has not been sent',
    validators: {
        isEmail: function (value) {
            return value.length && value.trim().match(/.+[^@]@[_a-z\d\-]+\.+[a-zР°-СЏ][a-zР°-СЏ]?[a-zР°-СЏ]$/i);
        },
        isLink: function (value) {
            return value.length && value.trim().match(/^(https?:\/\/)([\da-z\.-]+)\.([a-z]{2,6}\.?)(\/[\w\.]*)*\/?((.*)?(#[\w\-]+)?)?$/);
        },
        isNotEmpty: function (value) {
            return value.length;
        }
    },
    init: function () {
        this.bindEvents();
        this.successMessage = $('.j-form_success_message').html();
        this.errorMessage = $('.j-form_error_message').html();
    },
    bindEvents: function () {
        var self = this;
        $(document).on('focus change keyup', '.m-form_error',
            function (e) {
                if (e.type == 'keyup' && e.keyCode == 13) return false;
                self.clearErrors($(this).closest('form'));
            }).on('submit', '.j-form', function () {
            var $form = $(this);
            if (self.validate($form)) {
                self.ajaxSubmit($form, function (res) {
                    if (!res.errors) {
                        self.displayMessage($this);
                    }
                });
            }
            return false;
        })
    },
    validate: function ($form) {
        var self = this,
            $required = $form.find('input.j-required, textarea.j-required, select.j-required'),
            $email = $form.find('[name*="email"]'),
            $link = $form.find('[name*="link"]'),
            valid = true;

        self.clearErrors($form);
        if ($email.length) {
            $email.each(function () {
                var $this = $(this);
                if (!self.validators.isEmail($this.val())) {
                    valid = false;
                    self.displayError($this.closest('.j-group'), 'Ошибочка вышла, исправьте!');
                }
            });
        }
        if ($link.length) {
            $link.each(function () {
                var $this = $(this);
                if (!$this.val().trim().match(/^https?:\/\//)) {
                    $this.val('http://' + $this.val());
                }
                if ($this.hasClass('j-required')) {
                    if (!self.validators.isLink($this.val())) {
                        valid = false;
                        self.displayError($this.closest('.j-group'), 'Ошибочка вышла, исправьте!');
                    }
                }
            });
        }
        if ($required.length) {
            $required.each(function () {
                var $this = $(this);
                if (!self.validators.isNotEmpty($this.val())) {
                    valid = false;
                    self.displayError($this.closest('.j-group'), 'Вы забыли заполнить поле');
                }
            });
        }
        return valid;
    },
    clearErrors: function ($form, $label) {
        $form.find('.m-form_error').removeClass('m-form_error').find('.g-form_error_mess').remove();
    },
    displayError: function ($label, message) {
        $label.addClass('m-form_error');
        $errorBlock = $label.find('.g-form_error_mess');
        if (!$errorBlock.length) {
            $errorBlock = $('<span class="g-form_error_mess"></span>');
            $errorBlock.appendTo($label);
        }
        $errorBlock.text(message);
    },
    displayMessage: function ($form, message) {
        $form.addClass('m-form_hidden').next('.j-message').html(message).addClass('m-message_visible');
    },
    ajaxSubmit: function ($form) {
        var self = this;
        var data = $form.serializeArray();
        if ($form.data('vacancy_id')) {
            data.push({name: 'vacancy_id', value: $form.data('vacancy_id')});
        }
        $.ajax({
            url: $form.attr('action'),
            data: data,
            method: 'POST',
            success: function (response) {
                self.displayMessage($form, self.successMessage);
            },
            error: function (response) {
                self.displayMessage($form, self.errorMessage);
            }
        });
    }
};

$(window).load(function () {
    window.loaded = true;
});
if (jQuery.isReady) {
    Main.init();
} else {
    $(function () {
        Main.init();
    });
}
;