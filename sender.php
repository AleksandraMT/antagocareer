<?php
class TEmail {
    private $sender = 'antago@i.ua';
    private $receiver = 'antago@i.ua';
    private $subject = 'Запрос с сайта antagosoft.ru';
    private $body = '';
    private $headers = "MIME-Version: 1.0\r\nContent-type: text/html;";
    private $formData;
    private $vacancySubjects = [
        1 => 'Запрос с сайта antagosoft.ru на вакансию front-end',
        2 => 'Запрос с сайта antagosoft.ru на вакансию php-разработчик',
        3 => 'Запрос с сайта antagosoft.ru на вакансию javascript',
    ];
    private $vacancyLabels = [
        'name' => 'Имя',
        'email' => 'E-mail',
        'link' => 'Ссылка на резюме',
    ];
    private $contactLabels = [
        'name' => 'Имя',
        'email' => 'E-mail',
        'message' => 'Сообщение',
    ];
    
    function compose($labels){
        if (isset($this->formData['vdata']) && !empty($this->formData['vdata'])) {
            /* this is value from hidden field in forms, which users should not see, if it was filled than
            user is probably bot */
            die;
        }
        foreach ($labels as $attribute => $label) {
            if (!isset($this->formData[$attribute])) {
                continue;
            }
            $this->body.= '<div><b>'.$label.':</b> '.$this->formData[$attribute].'</div>';  
        }
    }
    
    function load(){
        if (isset($_POST['Contact'])) {
            $this->formData = $_POST['Contact'];
            $this->subject = $this->subject.' (клиент)';
            $this->compose($this->contactLabels);
            return true;
        }
        if (isset($_POST['Vacancy'])) {
            $this->formData = $_POST['Vacancy'];
            if (isset($this->vacancySubjects[$_POST['vacancy_id']])) {
                $this->subject = $this->vacancySubjects[$_POST['vacancy_id']];
            }
            $this->compose($this->vacancyLabels);
            return true;
        }
        return false;
    }
    
    function reportError($error){
        http_response_code(400);
        throw new Exception($error);
    }

    function send(){
        if (empty($this->body)) {
            return false;
        }
        $this->headers .= ' charset='.mb_detect_encoding($this->body)."\r\n";
        return mail($this->receiver, $this->subject, $this->body, $this->headers);
    }
}

$emailgo = new TEmail;
if (!$emailgo->load()) {
    $emailgo->reportError('no data received');
}
if(!$emailgo->send()) {
    $emailgo->reportError('mail has not been sent');
}
?>